# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = '質点の運動方程式を解くスクリプト'
copyright = '2022, Yuta Teshima'
author = 'Yuta Teshima'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',  # docstringからドキュメントの取り込み
    'sphinx.ext.napoleon', # Googleスタイルのdocstringからのドキュメントの読み込み
    'sphinx.ext.viewcode', # ハイライト済みのソースコードへのリンクの追加
    'sphinx.ext.todo',     # Todoアイテムのサポート
    'matplotlib.sphinxext.plot_directive', # Matplotlib のグラフの埋め込み
]

# autodocがPythonのスクリプトを見つけられるようにする
# サーバー上でも上手く動作するように，相対パスで指定する
# 絶対パスを得たい場合は， ``os.path.abspath`` メソッドを用いる．
import os, sys
sys.path.insert(0, os.path.abspath("../"))

# autodocの設定
# メソッドやプロパティが「ソースコードにかかれている順番で (bysource)」
# 出力されるようにする・
# デフォルトの設定だとアルファベット順に出力される
autodoc_member_order = 'bysource'

# napoleonの設定
# Google形式のDocstringを採用する．
napoleon_google_docstring = True
napoleon_numpy_docstring = False

# todosの設定
todo_include_todos = True
todo_emit_warnings = True # コンパイルしたときにWarningを出す
todo_link_only = True

# Matplotlib埋め込みの設定
plot_html_show_source_link = False
plot_html_show_formats = False

# MathJaxの設定
# physicsパッケージは便利なのでusepackageする．
mathjax3_config = {
    "loader": {
        "load": ["[tex]/physics"]
    },
    "tex": {
        "packages": {
            "[+]": ["physics"]
        }
    },
}

# LaTeXの数式設定
latex_elements = {
    'extrapackages': '\\usepackage{amsmath, amssymb, physics}',
    'papersize': 'a4paper',
}

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

language = 'ja'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'pydata_sphinx_theme'
html_static_path = ['_static']
