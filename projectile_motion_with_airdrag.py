"""
空気抵抗のある斜方投射を計算し，到達距離が最大となる投射角度を計算するスクリプト

実行結果
========

.. code-block::

        $ python  projectile_motion_with_airdrag.py 
        running 10.0 deg        x_end = 2.269 m
        running 15.0 deg        x_end = 2.855 m
        running 20.0 deg        x_end = 3.181 m
        running 25.0 deg        x_end = 3.373 m
        running 30.0 deg        x_end = 3.435 m
        running 35.0 deg        x_end = 3.383 m
        running 40.0 deg        x_end = 3.268 m
        running 45.0 deg        x_end = 3.089 m
        running 50.0 deg        x_end = 2.864 m
        Projection angle with the longest reach: 30 deg
        $

.. plot:: ../projectile_motion_with_airdrag.py

   実行例

See Also:
    :py:class:`~masspoint_system.MassPoint`,
    :py:class:`~masspoint_system.EquationOfMotion`

プロパティ
==========
"""

import math

import matplotlib.pyplot as plt

from masspoint_system import MassPoint, EquationOfMotion

MASS = 1.0
"""物体の質量"""

X0 = [0.0, 0.0]
"""物体の初期座標"""

V0_ABS = 10.0
"""物体の初期速度の絶対値"""

ANGLES = [angle for angle in range(10, 55, 5)]
"""調べる角度のリスト"""

G_ACCEL = 9.81
"""重力加速度 (:math:`\mathrm{m/s^2}`)"""

AIRDRAG_COEF = 2.0
"""粘性抵抗の係数 (:math:`\mathrm{N\cdot s/m}`)"""

T_STEP = 1e-3
"""シミュレーションの時間刻み (s)"""

T_END = 2 * V0_ABS / G_ACCEL  
"""シミュレーション終了時間

シミュレーション終了時間は，空気抵抗なく，真上に投げたときに地面に再びつく時間に設定．
この程度の値に設定しておけば，地面に到達するまえにシミュレーションが終了することはない．
"""

def projectile_motion_with_air_drag(angle: float) -> tuple[list[float], list[float], list[float]]:
    """空気抵抗のある斜方投射のシミュレーションを行う

    Args:
        angle (float): 投射する角度 (deg)

    Returns:
        tuple[list[float], list[float], list[float]]: 時間履歴，x方向の履歴，y方向の履歴
    """
    v0 = [
        V0_ABS * math.cos(math.radians(angle)),  # x方向の初速
        V0_ABS * math.sin(math.radians(angle))  # y方向の初速
    ]

    def airdrag(obj: EquationOfMotion) -> list[float]:
        """空気抵抗のある外力項

        Args:
            obj (EquationOfMotion): 運動方程式のクラスのインスタンス

        Returns:
            list[float]: 外力項

        SeeAlso:
            :py:prop:`~masspoint_system.EquationOfMotion.external_force`
        """
        return [
            -AIRDRAG_COEF * obj._masspoint.velocity[0],  # x方向の外力
            -AIRDRAG_COEF * obj._masspoint.velocity[1] - obj._masspoint.mass * G_ACCEL  # y方向の外力
        ]

    m = MassPoint(MASS, X0, v0)
    eom = EquationOfMotion(masspoint=m, external_force=airdrag)
    eom.run(t_step=T_STEP, t_end=T_END)

    return eom.time_history, eom.position_history[0], eom.position_history[1]


def main() -> None:
    """角度 ``ANGLES`` を走査し，x方向の到達点を求め標準出力ストリームに出力する．

    すべての角度について走査が終わった後に，グラフを出力する．
    """
    x_end_result = []
    for angle in ANGLES:
        print(f'running {angle:3.1f} deg', end='\t')
        _, x_raw, y_raw = projectile_motion_with_air_drag(angle)
        x_filtered = []
        y_filtered = []

        for x, y in zip(x_raw, y_raw):
            if y >= 0:
                x_filtered.append(x)
                y_filtered.append(y)
            else:
                break

        x_end_result.append(x_filtered[-1])
        print(f'x_end = {x_filtered[-1]:.3f} m')
        plt.plot(x_filtered, y_filtered, label=f'{angle:.1f} deg')

    max_index = max(range(len(x_end_result)), key=lambda i: x_end_result[i])
    print(f'Projection angle with the longest reach: {ANGLES[max_index]} deg')

    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
