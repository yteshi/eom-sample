from sys import float_info
from typing import Union, final
import copy


class MassPoint:
    """質点クラス

    Args:
        mass (float):
            質点の質量 (kg)
        initial_position (list(float)):
            質点の初期座標 (m)
        initial_velocity (list(float)):
            質点の初期速度 (m/s)
    """

    def __init__(self, mass: float, initial_position: list[float], initial_velocity: list[float]):
        self._mass = mass
        self._initial_position = initial_position
        self._initial_velocity = initial_velocity
        self._position = copy.deepcopy(self._initial_position)
        self._velocity = copy.deepcopy(self._initial_velocity)

    @property
    def mass(self) -> float:
        """質点の質量
        
        このプロパティは初期化時にのみ設定でき，あとから変更することはできない．
        """
        return self._mass

    @property
    def initial_position(self) -> list[float]:
        """質点の初期位置"""
        return self._initial_position

    @property
    def initial_velocity(self) -> list[float]:
        """質点の初期速度"""
        return self._initial_velocity

    @property
    def position(self) -> list[float]:
        """質点の位置
        
        Raises:
            TypeError: 新しい位置の配列の長さが正しくない場合に発生する
        """
        return self._position

    @position.setter
    def position(self, nxt_position: list[float]) -> None:
        if len(self._position) != len(nxt_position):
            raise TypeError('位置の型に誤りがあります．')

        self._position = nxt_position

    @property
    def velocity(self) -> list[float]:
        """質点の速度
        
        Raises:
            TypeError: 新しい速度の配列の長さが正しくない場合に発生する
        """
        return self._velocity

    @velocity.setter
    def velocity(self, nxt_velocity: list[float]) -> None:
        if len(self._velocity) != len(nxt_velocity):
            raise TypeError('速度の型に誤りがあります．')

        self._velocity = nxt_velocity
