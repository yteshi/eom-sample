from typing import Callable, Union

from . import MassPoint


class EquationOfMotion:
    """1体問題の運動方程式のクラス．

    運動方程式を単純な離散化によってシミュレーションする．
    離散化した計算は「 **蛙飛び法** 」によって行い，

    .. math:: 

       v[i] &= v[i-1] + a[i] \\Delta t

       x[i] &= x[i-1] + \\frac{1}{2}\\pqty{v[i] + v[i-1]}\\Delta t
       
    である．

    Args:
        masspoint (masspoint_system.MassPoint):
            質点
        external_force (Callable):
            外力項
    """

    def __init__(self, masspoint: MassPoint, external_force: Callable[..., list[float]]):
        self._masspoint = masspoint
        self._position_history: list[list[float]] = [[]]
        self._velocity_history: list[list[float]] = [[]]
        self._time_history: list[float] = []
        self.external_force = external_force

    @property
    def position_history(self) -> list[list[float]]:
        """質点の位置履歴"""
        return self._position_history

    @property
    def velocity_history(self) -> list[list[float]]:
        """質点の速度履歴"""
        return self._velocity_history

    @property
    def time_history(self) -> list[float]:
        """時間の履歴"""
        return self._time_history

    def run(self, t_step: float, t_end: float) -> None:
        """運動方程式の実行
        
        Args:
            t_step (float):
                時間の刻み幅
            t_end (float):
                シミュレーション終了時刻
        
        Raises:
            TypeError: 時間刻みと終了時刻のいずれかが0または負の数である場合，
                       終了時刻が時間刻みよりも小さい場合に発生する．
        """

        if t_step <= 0 or t_end <= 0:
            raise TypeError('時間刻みと終了時刻は正の数でなければいけません．')

        if t_step > t_end:
            raise TypeError('終了時刻は時間刻みよりも大きくなければいけません．')

        t = 0.0
        self._position_history = [[x] for x in self._masspoint.initial_position]
        self._velocity_history = [[v] for v in self._masspoint.initial_velocity]
        while t < t_end:
            # --------------------------------
            # 位置の更新
            # --------------------------------
            nxt_pos = [
                x + 0.5 * v * t_step
                for x, v in zip(self._masspoint.position, self._masspoint.velocity)
            ]
            self._masspoint.position = nxt_pos

            # --------------------------------
            # 速度の更新
            # --------------------------------
            force = self.external_force(self)
            accel = [ff / self._masspoint.mass for ff in force]
            nxt_vel = [v + a * t_step for v, a in zip(self._masspoint.velocity, accel)]
            self._masspoint.velocity = nxt_vel

            # --------------------------------
            # 位置の更新
            # --------------------------------
            nxt_pos = [
                x + 0.5 * v * t_step
                for x, v in zip(self._masspoint.position, self._masspoint.velocity)
            ]
            self._masspoint.position = nxt_pos

            for x_list, new_x in zip(self._position_history, self._masspoint.position):
                x_list.append(new_x)

            for v_list, new_v in zip(self._velocity_history, self._masspoint.velocity):
                v_list.append(new_v)
            
            self._time_history.append(t)
            t += t_step
