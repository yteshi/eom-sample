import pytest

from masspoint_system import MassPoint


class TestMassPoint:
    """MassPointクラスのテストを行う

    See Also:
        :py:class:`~masspoint_system.MassPoint`
    """

    def test__create_instance(self) -> None:
        """インスタンスを正しく作れるか"""
        mp = MassPoint(1.0, [0, 0], [5, 5])
        assert type(mp) == MassPoint
