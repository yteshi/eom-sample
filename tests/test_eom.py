import pytest

from masspoint_system import MassPoint, EquationOfMotion

import matplotlib.pyplot as plt


class TestEom:
    """運動方程式シミュレーションのテスト
    
    See Also:
        :py:class:`~masspoint_system.EquationOfMotion`
    """

    @pytest.fixture
    def init_masspoint(self) -> None:
        self.masspoint = MassPoint(1, [0, 0], [10, 9.81])
        assert type(self.masspoint) == MassPoint

    @pytest.fixture
    def test__create_instance(self, init_masspoint: None) -> None:
        def gravitational_force(obj: EquationOfMotion) -> list[float]:
            g = -9.81
            return [0, obj._masspoint.mass * g]

        self.eom = EquationOfMotion(self.masspoint, gravitational_force)
        assert type(self.eom) == EquationOfMotion

    def test__simulate_parabolic_motion(
            self, test__create_instance: None) -> tuple[list[float], list[float]]:
        """放物運動をシミュレーションできるかを確かめる
        
        Returns:
            tuple[list[float], list[float]]: x座標の履歴，y座標の履歴
        """
        self.eom.run(t_step=0.01, t_end=2)
        x = self.eom.position_history[0]
        y = self.eom.position_history[1]
        assert abs(20.0 - x[-1]) < 1e-6
        assert abs(0 - y[-1]) < 1e-6

        print(x[-1], y[-1])
        plt.plot(x, y)
        plt.show()

        return x, y

    def test__simulate_vibration(self) -> tuple[list[float], list[float]]:
        """バネの単振動をシミュレーションできるかを確かめる

        Returns:
            tuple[list[float], list[float]]: 時間の履歴, x座標の履歴
        """
        def spring_force(obj: EquationOfMotion) -> list[float]:
            k = 1
            return [-k * obj._masspoint.position[0]]
        
        masspoint = MassPoint(1, [1], [0])
        eom = EquationOfMotion(masspoint, spring_force)
        eom.run(t_step = 0.01, t_end = 3.14159 * 3)
        x = eom.position_history[0]
        t = [0.01 * i for i in range(len(x))]
        plt.plot(t, x)
        plt.show()

        return t, x
